const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

canvas.height = window.innerHeight;
canvas.width = window.innerWidth;

const height_canvas = canvas.height;
const width_canvas = canvas.width;

const quantityObject = 250;
const moveObjectOnMouse = 60;
let objectParticle = [];


for(let i = 0; i < quantityObject; i++){

		objectParticle[i] = {
			number: [i],
			size: 3,
			bg_color: '#0b5351',
			position_x: Math.floor(Math.random() * width_canvas) + 20,
			position_y: Math.floor(Math.random() * height_canvas) + 20,
			speed_x: .2,
			speed_y: .2,
			on_mouse: false,
			temp_speed_x: 2,
			temp_speed_y: 2,
			bufor_meet: []
		}
}


function drawCanvas(){
	ctx.fillStyle = '#001427';
	ctx.fillRect(0,0, width_canvas, height_canvas);
};


// create wheels
function wheels(){

	for(let i = 0; i < quantityObject; i++){

		// ctx.beginPath();
		// ctx.arc(objectParticle[i].position_x, objectParticle[i].position_y, objectParticle[i].size, 0, 2 * Math.PI, false);
		// ctx.fillStyle = objectParticle[i].bg_color;
		// ctx.fill();

		ctx.fillStyle = objectParticle[i].bg_color;
	    ctx.fillRect(objectParticle[i].position_x, objectParticle[i].position_y, objectParticle[i].size, objectParticle[i].size);

	    // change position to movment object
	    if(i % 2 === 0){
	    	objectParticle[i].position_x += objectParticle[i].speed_x;
	    	objectParticle[i].position_y += objectParticle[i].speed_y;
	    }else{
	    	objectParticle[i].position_x += -objectParticle[i].speed_x;
	    	objectParticle[i].position_y += -objectParticle[i].speed_y;
	    }
	    

	    // if object going after canvas, change position to return object
	    if(objectParticle[i].position_x <= -30 || objectParticle[i].position_x >= width_canvas + 20){
	    	objectParticle[i].speed_x = -objectParticle[i].speed_x;
	    }

	    if(objectParticle[i].position_y <= -30 || objectParticle[i].position_y >= height_canvas + 20){
	    	objectParticle[i].speed_y = -objectParticle[i].speed_y;
	    }

	}

};


function drawLineBetweenParticles(){

	var temp_size_line = 180;

	for (var i = 0; i !== objectParticle.length - 1; i++) {
		for (var j = i + 1; j !== objectParticle.length; j++) {



			if (objectParticle[i].position_x < objectParticle[j].position_x + objectParticle[j].size + temp_size_line  &&
				objectParticle[i].position_x + objectParticle[i].size + temp_size_line > objectParticle[j].position_x &&
				objectParticle[i].position_y < objectParticle[j].position_y + objectParticle[j].size + temp_size_line && 
				objectParticle[i].position_y + objectParticle[i].size > objectParticle[j].position_y) {

	  			ctx.beginPath();
	  			ctx.moveTo(objectParticle[i].position_x, objectParticle[i].position_y);
		        ctx.lineTo(objectParticle[j].position_x, objectParticle[j].position_y);
		        ctx.lineWidth = 1;
		        ctx.strokeStyle = 'rgba(11 , 83 , 81 , .1 )';
		        ctx.stroke();
		        ctx.closePath();
	  		}

		}
	}


}


function detectColision(){

	for (var i = 0; i !== objectParticle.length - 1; i++) {
	  for (var j = i + 1; j !== objectParticle.length; j++) {

	  	if (objectParticle[i].position_x < objectParticle[j].position_x + objectParticle[j].size  &&
				objectParticle[i].position_x + objectParticle[i].size > objectParticle[j].position_x &&
				objectParticle[i].position_y < objectParticle[j].position_y + objectParticle[j].size && 
				objectParticle[i].position_y + objectParticle[i].size > objectParticle[j].position_y) {

	  		if(objectParticle[i].bufor_meet.indexOf(j) === -1 && objectParticle[j].bufor_meet.indexOf(i) === -1){
	  				objectParticle[i].bg_color = 'green';

					objectParticle[i].speed_x = -objectParticle[i].speed_x;
                    objectParticle[i].speed_y = -objectParticle[i].speed_y;

                    objectParticle[j].speed_x = -objectParticle[j].speed_x;
                    objectParticle[j].speed_y = -objectParticle[j].speed_y;

                
                    objectParticle[i].bufor_meet.push(j);
                    objectParticle[j].bufor_meet.push(i);
	  		}

	  	}else{
	  		var temp = objectParticle[i].bufor_meet.indexOf(j);

	  		if( temp !== -1){
	  			objectParticle[i].bufor_meet.slice(temp, 1);
	  		}

	  		var temp = objectParticle[j].bufor_meet.indexOf(i);

	  		if( temp !== -1){
	  			objectParticle[j].bufor_meet.slice(temp, 1);
	  		}
	  	}


	  }
	}

};



canvas.addEventListener('click', function(event){
	let rect = canvas.getBoundingClientRect();
	let client_x = event.clientX - rect.left;
	let client_y = event.clientY - rect.top;


		for(let i = 0; i < quantityObject; i++){
			
			// after click change postion
			if(objectParticle[i].position_x <= client_x){

				objectParticle[i].speed_x = 1;
				objectParticle[i].position_x += .1;
				
			}

			if(objectParticle[i].position_y <= client_y){

				objectParticle[i].speed_y = 1;
				objectParticle[i].position_y += .1;
			
			}


			if(objectParticle[i].position_x >= client_x){

				objectParticle[i].speed_x = -1;
				objectParticle[i].position_x += .1;

			}



			if(objectParticle[i].position_y >= client_y){

				objectParticle[i].speed_y = -1;
				objectParticle[i].position_y += .1;
				
			}

			
		}
			

		

});


function startDrawing(){
	drawCanvas();
	wheels();
	drawLineBetweenParticles();
	detectColision();
}



setInterval(startDrawing, 1000/60);
